import json

class YelpParser:
  def __init__(self, filename):
    self._jobjects = {}
    with open(filename, 'rb') as f:
      for i, line in enumerate(f):
        temp = json.loads(line)
        self._jobjects[temp["business_id"]] = temp
      #print "Size: ", i+1

  def get_json_objects(self):
    return self._jobjects


if __name__ == "__main__":
  filename1 = "yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_business.json"
  filename2 = "yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_review.json"
  jsonBusiness = YelpParser(filename1).get_json_objects()
  jsonReview = YelpParser(filename2).get_json_objects()
  #gen = (jsonBusiness[ident] for ident, ob in jsonReview.items())
  gen = ((ob["stars"], jsonBusiness[id]["full_address"]) for id, ob in jsonReview.items())
  for n in gen:
    print n
