import numpy as np
from scipy.ndimage.interpolation import rotate
from math import atan2, cos, sin, pi, degrees, sqrt
import random
from collections import Counter

ELLIPSE_WIDTH_RATIO = 1/2.

class Distribution:
  def __init__(self, indices, counts, inverted=False):
    assert len(indices) == len(counts)
    self.dist = np.array(counts, dtype='double', copy=True)
    self.dist /= np.sum(self.dist)
    self.indices = np.copy(indices)
    if inverted:
      self.dist = 1-self.dist

  def query(self, prob):
    rtot = 0
    for i, r in enumerate(self.dist):
      rtot += r
      if rtot > prob:
        return self.indices[i]
    return self.indices[i]

  def queryRemove(self, probs):
    ret = np.zeros((len(probs),), dtype='int')
    for j, prob in enumerate(probs):
      rtot = 0
      for i, r in enumerate(self.dist):
        rtot += r
        if rtot > prob:
          break
      ret[j] = self.indices[i]
      self.indices = np.delete(self.indices, i)
      self.dist = np.delete(self.dist, i)
      self.dist /= np.sum(self.dist) #renormalize with element removed
    return ret


def euclidean_dist(p1, p2):
  p1 = TP[p1]
  p2 = TP[p2]
  return sqrt(sum(map(lambda x,y: (x-y)**2, p1, p2)))

def getLineAngle(p1, p2):
  p1 = TP[p1]
  p2 = TP[p2]
  return atan2(p2[1] - p1[1], p2[0] - p1[0])

def createEllipse(line, widthRatio):
  angle = getLineAngle(*line)
  lineDist = euclidean_dist(*line)
  return (sum(TP[line])/2., (lineDist/2, widthRatio*lineDist/2), angle)

def getPointsInEllipsis(points, elli):
  (h, k), (rx, ry), a = elli
  rx2 = rx**2
  ry2 = ry**2

  x = TP[points][:, 0]
  y = TP[points][:, 1]

  #Return what is within ellipsis equation (with rotation)
  return points[((cos(a)*(x-h) + sin(a)*(y-k))**2/float(rx2)) +
                ((sin(a)*(x-h) - cos(a)*(y-k))**2/float(ry2)) <= 1]

def findPathOfK(line, points, categoriesCount, k):
  def rotate(points, angle):
    rotMat = np.asarray([[cos(angle), -sin(angle)],
                         [sin(angle), cos(angle)]], dtype="double")
    return np.transpose(np.dot(rotMat, np.transpose(points)))

  k -= 2 #Beginning and ending are provided
  #Return line if path already has enough
  if k <= 0:
    return line
  #center = np.min(TP[line], 0)
  center = TP[line[0]]
  angle = getLineAngle(*line)

  #Remove begin and end from choices
  b = (points != line[0]) & (points != line[1])
  rest = np.asarray(range(len(points)))[b]
  #Only continue if we can continue on something
  if len(rest) == 0:
    return line
  #Create normalized array
  RTP = TP[points]
  RTP -= center
  RTP = rotate(RTP, -angle)

  #Split points up in section along the line
  sections = []
  dist = euclidean_dist(*line)
  inc = dist/float(k)
  for x in np.linspace(inc, dist, num=k):
    b = RTP[rest][:, 0] < x
    sections.append(rest[b])
    rest = rest[~b]
    if len(rest) == 0:
      break

  #Recursively try to grab one bar from each section, grabbing more from others if no
  #bars found in a section
  def choose_path(i, needed, total):
    #Base Case
    if i >= len(sections):
      return (total, []) #What did we not satisfy going forward?

    def choose_portion(i, needed):
      origLen = len(sections[i])
      if needed == 0 or origLen == 0:
        return (needed, [])
      nsample = min(needed, origLen) #Can only choose as many as we have available
      chosen = Distribution(sections[i], categoriesCount[sections[i]]).queryRemove(np.random.random(nsample))

      sections[i] = sections[i][np.invert(np.in1d(sections[i], chosen))] #Remove these so we do not choose them in future
      return (needed-nsample, chosen)

    #Satisfy immediatly needed
    left, new = choose_portion(i, needed)
    #Recurse for rest
    left, rest = choose_path(i+1, left+1, total - len(new))
    if len(new) > 0:
      rest.append(new)
    #Retry to satisfy those not satisfied further
    left, new = choose_portion(i, left)
    if len(new) > 0:
      rest.append(new)

    return (left, rest)

  unsatisfiable, path = choose_path(0, 1, k)
  if unsatisfiable > 0:
    print "Could not find %d bars." % unsatisfiable

  def sortPoints(flat):
    rotatedSortedList = np.argsort(np.asarray(RTP[flat][:,0]))
    return np.asarray(flat[rotatedSortedList])


  #Ensure path is logically sequenced after possible compensation
  flatPath = np.asarray([p for ps in path for p in ps])
  path = sortPoints(flatPath)
  #Convert back to global indexing
  path = points[path]
  #Add on beginning and end points
  path = np.concatenate([line[:1]]+[path]+[line[1:]])

  return path

def GetCrawl(nbars, begining, end, data, points, categoriesCount):
  global TP
  TP = data
  points = np.asarray(range(len(data)))
  line = [begining, end] if TP[begining][0] < TP[end][0] else [end, begining]
  line = np.asarray(line)

  #create ellipse
  ellipsis = createEllipse(line, ELLIPSE_WIDTH_RATIO)

  #Get points in ellipsis
  #TODO: Use KD tree to narrow down before ellipse?
  after = getPointsInEllipsis(points, ellipsis)

  #Find the path
  path = findPathOfK(line, after, categoriesCount, nbars)
  assert len(path) == len(set(path))
  if path[0] == begining:
    return path
  else:
    return path[::-1]


if __name__ == "__main__":
  import matplotlib.pyplot as plt
  from matplotlib.patches import Ellipse
  from YelpParser import YelpParser

  def plotTest(points, line, chosen, path, ellipsis):
    e = Ellipse(xy=ellipsis[0], width=ellipsis[1][0]*2, height=ellipsis[1][1]*2,
        angle=degrees(ellipsis[2]))
    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal')
    ax.add_artist(e)
    plt.plot(TP[points][:,0], TP[points][:,1], linestyle='', marker='o', color="r")
    plt.plot(TP[chosen][:,0], TP[chosen][:,1], linestyle='', marker='o', color="g")
    plt.plot(*zip(*TP[line]), linestyle='-', marker='s', color='k')
    plt.plot(*zip(*TP[path]), linestyle='-', marker='*', color='c')
    plt.title("Test Crawl on Las Vegas")
    plt.xlabel("Longitude")
    plt.ylabel("Latitude")
    plt.savefig("tmp.png", bbox_inches='tight')
    plt.show()

  #Read data for bars in Nevada
  filename1 = "yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_business.json"
  jsonBusiness = YelpParser(filename1).get_json_objects()
  nevadaBars = [(i, ob) for i, ob in jsonBusiness.items() if ob["state"] == "NV" and "Bars" in ob["categories"]]
  categories = np.asarray([ob["categories"] for _, ob in nevadaBars])

  #Place data into what we will operate on
  data = np.asarray([(ob["longitude"], ob["latitude"]) for _, ob in nevadaBars])
  TP = data
  points = np.asarray(range(len(data))) #index array
  categorieCount = Counter((c for cats in categories for c in cats))
  categorieCount = np.asarray([min((categorieCount[c] for c in cats)) for cats in categories])

  #Find a line
  #line = np.asarray(random.sample(points, 2))
  line = np.asarray([5, 0])
  line = line[np.argsort(TP[line], axis=0)[:,0]]

  #create ellipse
  ellipsis = createEllipse(line, ELLIPSE_WIDTH_RATIO)

  #plot what is in and out of ellipse
  #TODO: Use KD tree to narrow down before ellipse?
  after = getPointsInEllipsis(points, ellipsis)

  #Find the path
  path = findPathOfK(line, after, categorieCount, 5)
  print categories[path]

  #plot the results
  plotTest(points, line, after, path, ellipsis)

