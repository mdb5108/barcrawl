import pickle
import numpy as np
from YelpParser import YelpParser
from collections import Counter

def init_state():
  print "Initializing Bar Crawl"
  #filename = settings.YELP_FOLDER+"yelp_academic_dataset_business.json"
  filename = "yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_business.json"
  jsonBusiness = YelpParser(filename).get_json_objects()
  tmp = [(bid, (ob["longitude"], ob["latitude"]), ob["name"], ob["full_address"], ob["categories"])
      for bid, ob in jsonBusiness.items()
      if "Bars" in ob["categories"]]
      #if ob["state"] == "NV" and "Bars" in ob["categories"]]
  idToIndex = {bid : i for i, (bid, _, _, _, _) in enumerate(tmp)}
  barIds = np.asarray([bid for bid, _, _, _, _  in tmp])
  barPoints = np.asarray([p for _, p, _, _, _ in tmp])
  barNames = np.asarray([n for _, _, n, _, _ in tmp])
  barAddresses = np.asarray([a for _, _, _, a, _ in tmp])
  barIndices = np.asarray(range(len(tmp)))
  barUniqueCat = Counter((c for _, _, _, _, cats in tmp for c in cats))
  barUniqueCat = np.asarray([min(((barUniqueCat[c], c) for c in cats),key=lambda x: x[0])
                             for _,_,_,_, cats in tmp])
  print "Length of bar array: ", len(tmp)

  with open("cache/barIds.pkl", 'wb') as f:
    pickle.dump(barIds, f)
  with open("cache/barAddresses.pkl", 'wb') as f:
    pickle.dump(barAddresses, f)
  with open("cache/barNames.pkl", 'wb') as f:
    pickle.dump(barNames, f)
  with open("cache/barPoints.pkl", 'wb') as f:
    pickle.dump(barPoints, f)
  with open("cache/barIndices.pkl", 'wb') as f:
    pickle.dump(barIndices, f)
  with open("cache/idToIndex.pkl", 'wb') as f:
    pickle.dump(idToIndex, f)
  with open("cache/barUniqueCat.pkl", 'wb') as f:
    pickle.dump(barUniqueCat, f)
init_state()

