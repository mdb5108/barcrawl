from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include('barcrawl.urls', namespace='barcrawl')),
    #url(r'^barcrawl/', include('barcrawl.urls', namespace='barcrawl')),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
