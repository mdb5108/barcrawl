#TODO: filter out bars at same location
from django.http import HttpResponse
from django.shortcuts import render
from django.conf import settings

import pickle
import numpy as np
import json
import traceback

import FindCrawl


def get_barIds():
  with open(settings.CACHE_FOLDER+"barIds.pkl", 'rb') as f:
    return pickle.load(f)
def get_barAddresses():
  with open(settings.CACHE_FOLDER+"barAddresses.pkl", 'rb') as f:
    return pickle.load(f)
def get_barNames():
  with open(settings.CACHE_FOLDER+"barNames.pkl", 'rb') as f:
    return pickle.load(f)
def get_barPoints():
  with open(settings.CACHE_FOLDER+"barPoints.pkl", 'rb') as f:
    return pickle.load(f)
def get_barIndices():
  with open(settings.CACHE_FOLDER+"barIndices.pkl", 'rb') as f:
    return pickle.load(f)
def get_idToIndex():
  with open(settings.CACHE_FOLDER+"idToIndex.pkl", 'rb') as f:
    return pickle.load(f)
def get_barUniqueCat():
  with open(settings.CACHE_FOLDER+"barUniqueCat.pkl", 'rb') as f:
    return pickle.load(f)


def index(request):
  return render(request, 'barcrawl/index.html', {})

def barcrawl(request):
  barIds = get_barIds()
  barAddresses = get_barAddresses()
  barNames = get_barNames()
  context = { 'namesAndAdd'  : zip(barIds, barAddresses, barNames) }
  return render(request, 'barcrawl/barcrawl.html', context)

def about(request):
  return render(request, 'barcrawl/about.html', {})

def method(request):
  return render(request, 'barcrawl/method.html', {})

def dataminejs(request):
  return render(request, 'barcrawl/datamining.js', {})

#def text(request, s):
#  return HttpResponse("You added this string: %s" % s)

def GetCrawl(request):
  """
  GetCrawl

  Takes in the beginning and end bars as unique business ids.  Also takes in k as the
  number of bars to hit along the path.

  Returns an array, starting at 'beginning', that is the bar crawl path found to end.
  """
  print "Finding Bar Crawl"
  #k = request.GET['k']
  k = 5
  beginning = request.GET['beginning']
  end = request.GET['end']
  print "beginning: %s, end: %s, k: %s" % (beginning, end, k)
  try:
    idToIndex = get_idToIndex()
    #e = "Couldn't parse int!"
    #k = int(k)
    e = "%s was not a valid business id!" % beginning
    beginning = idToIndex[beginning]
    e = "%s was not a valid business id!" % end
    end = idToIndex[end]
    e = "Unspecified error finding path between pubs!"
    
    barPoints = get_barPoints()
    barIndices = get_barIndices()
    barUniqueCat = get_barUniqueCat()
    barNames = get_barNames()
    if beginning == end:
      return HttpResponse(json.dumps([(lat, lng, name) for (lng, lat), name in
        zip(barPoints[[beginning, end]], barNames[[beginning, end]])]))
    crawlIds = FindCrawl.GetCrawl(k, beginning, end, barPoints, barIndices, barUniqueCat[:,0])
    crawl = [(lat, lng, name) for (lng, lat), name in zip(barPoints[crawlIds], barNames[crawlIds])]
    if len(set(((p1, p2) for p1, p2 in barPoints[crawlIds]))) != len(crawlIds) and len(set(crawl)) == len(crawl):
       print 'Found dup path!', crawl
    return HttpResponse(json.dumps(crawl))
  except Exception as exc:
    print e
    print traceback.format_exc()
    return HttpResponse(json.dumps([]))

