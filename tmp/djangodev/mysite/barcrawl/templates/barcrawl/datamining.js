var mapCanvas
//var mapOptions;
var map;
var lat = 44;
var lon = -78;
var latTO = 45;
var lonTO = -79;
var flightPlanCoordinates;
var flightPath;
var directionsDisplay;
var directionsService;
function initialize() {	
	directionsService = new google.maps.DirectionsService();
	directionsDisplay = new google.maps.DirectionsRenderer();
	var mapOptions = {
		zoom: 7,
		center: new google.maps.LatLng(41.850033, -87.6500523)
	};
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	directionsDisplay.setMap(map);
	//directionsDisplay.setPanel(document.getElementById('directions-panel'));

	var control = document.getElementById('control');
	control.style.display = 'block';
	map.controls[google.maps.ControlPosition.TOP_CENTER].push(control);

}

var prevCodeCtrl = 0;
function unvalidatectrl(evt){
	if (evt.ctrlKey){
		prevCodeCtrl = 0;
	}
}
function validatectrl(evt){
	if (evt.ctrlKey){
		prevCodeCtrl = 1;
	}
}
function validate(evt) {
	var theEvent = evt || window.event;
	var key = theEvent.keyCode || theEvent.which;
	key = String.fromCharCode( key );
	if (prevCodeCtrl == 1 && (evt.keyCode == "65" || evt.keyCode =="67")){
		
		return true;
	}
	if (evt.keyCode == "8" || evt.keyCode=="46" || evt.keyCode=="37" || evt.keyCode=="39" || evt.keyCode=="189"){
		
		return true;
	}
	var regex = /[0-9]|\./;
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		if(theEvent.preventDefault) theEvent.preventDefault();
	}
	

}

function calcRoute(){
	var start = document.getElementById('startLocation').value;
	var end = document.getElementById('endLocation').value;
        $.get('{% url 'barcrawl:GetCrawl' %}', {beginning: start, end: end}, function(result) {
          result = JSON.parse(result)
          if(result.length > 1) {
            var waypts = [];

            for(i = 1; i < result.length-1; i++) {
              waypts.push({
                location: new google.maps.LatLng(result[i][0],result[i][1]),
                stopover:true});
            }

            var request = {
              origin: new google.maps.LatLng(result[0][0], result[0][1]),
          destination: new google.maps.LatLng(result[result.length-1][0],
                                              result[result.length-1][1]),
          waypoints: waypts,
          optimizeWaypoints: false,
          travelMode: google.maps.TravelMode.DRIVING
            };
            console.log(request);
            directionsService.route(request, function(response, status){
              console.log(status);
              if (status == google.maps.DirectionsStatus.OK){
                directionsDisplay.setDirections(response);
                var route = response.routes[0];
                var summaryPanel = document.getElementById('directions-panel');
                summaryPanel.innerHTML = '';
                for(var i = 0; i < route.legs.length; i++){
                  var routeSegment = i+1;
                  summaryPanel.innerHTML += '<b>' + result[i][2] + ' -> ' + result[i+1][2] + '</b><br>';
                  summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                  summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                  summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
                }
              }	
            });
          }
        });
	
}


/*
function updateMap(){
	lat = document.getElementById("latitude").value;
	lon = document.getElementById("longitude").value;
	latTO = document.getElementById("latitudeTO").value;
	lonTO = document.getElementById("longitudeTO").value;
	var diflat = latTO - lat;
	var diflon = lonTO - lon;
	flightPlanCoordinates = [
		new google.maps.LatLng(lat,lon),
		new google.maps.LatLng(lat+(diflat*.3), lon+(diflon*.3)),
		new google.maps.LatLng(latTO -(diflat*.3), lonTO-(diflon*.3)),
		new google.maps.LatLng(latTO,lonTO)
	];
	flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		geodesic: true,
		strokeColor: '#FF0000',
		strokeOpacity: 1.0,
		strokeWight: 2
	});
	


}
*/
/*
function calcRoute(startI, endI){

	//var start = document.getElementById('start').value;
	var start = "4840 E Indian School Rd, AZ";
  var end = document.getElementById('end').value;
  var request = {
    origin: start,
    destination: end,
    travelMode: google.maps.TravelMode.DRIVING
  };
  renderDirections(request);
  var requestI = {
    origin: "joplin, mo",
    destination: "chicago, il",
    travelMode: google.maps.TravelMode.DRIVING
  };
  renderDirections(requestI);
  
  /*
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
  
  

  
 

}
*/

/*
function renderDirections(request){
	//directionsDisplay.setMap(map);
	directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
	});
	
}
function requestDirections(){
	var start = document.getElementById('start').value;
	var end = document.getElementById('end').value;
	var request = {
    origin: start,
    destination: end,
    travelMode: google.maps.TravelMode.DRIVING
	};
	renderDirections(request);
	/*
	directionsService.route({
		origin: start,
		destination: end,
		travelMode: google.maps.DirectionsTravelMode.DRIVING
	}, function(result){
		renderDirections(result);
	});
	


}
*/

/*
function calcRoute(){
	var start = document.getElementById('start').value;
	var end = document.getElementById('end').value;
	rDir(start, end);
}
*/

/*
function renderD(result){
	var directionsR = new google.maps.DirectionsRenderer;
	directionsR.setMap(map);
	directionsR.setDirections(result);
}
function rDir(start, end){
	directionsService.route({
		origin:start,
		destination: end,
		travelMode: google.maps.DirectionsTravelMode.DRIVING
	}, function(result){
		renderD(result);
	});
}
*/


