from django.conf.urls import patterns, url

import views

urlpatterns = patterns('',
    url(r'index.html$', views.index, name='index'),
    url(r'^$', views.index, name='index'),
    url(r'^about/$', views.about, name='about'),
    url(r'^method/$', views.method, name='method'),
    #url(r'^barcrawl/$', views.barcrawl, name='barcrawl'),
    url(r'^findcrawl/$', views.barcrawl, name='barcrawl'),
    url(r'^GetCrawl/$', views.GetCrawl, name='GetCrawl'),
    url(r'^datamining.js$', views.dataminejs, name='dataminejs'),
    #url(r'^categories/$', views.categories, name='categories'),
    #url(r'^GetAddresses/$', views.serve_test, name='GetAddress'),
    #url(r'^(?P<s>\S+)/text/$', views.text, name='text'),
    )
